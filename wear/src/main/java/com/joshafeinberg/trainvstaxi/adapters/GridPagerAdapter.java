package com.joshafeinberg.trainvstaxi.adapters;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.wearable.view.FragmentGridPagerAdapter;

import com.joshafeinberg.trainvstaxi.fragments.ActionFragment;
import com.joshafeinberg.trainvstaxi.fragments.CardFragment;
import com.joshafeinberg.trainvstaxi.models.GridPage;

import java.util.ArrayList;

public class GridPagerAdapter extends FragmentGridPagerAdapter {

    private ArrayList<GridPage> mRow;

    public GridPagerAdapter(FragmentManager fm, ArrayList<GridPage> row) {
        super(fm);
        mRow = row;
    }


    @Override
    public Fragment getFragment(int row, int col) {
        GridPage page = mRow.get(row);
        if (col == 0) {
            return CardFragment.create(page.getTitle(), page.getText(), page.getType());
        } else {
            return ActionFragment.create(page.getType(), page.getIcon(), page.getProductId());
        }
    }


    @Override
    public int getRowCount() {
        return mRow.size();
    }

    @Override
    public int getColumnCount(int row) {
        return 2;
    }
}