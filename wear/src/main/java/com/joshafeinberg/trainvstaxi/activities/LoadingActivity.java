package com.joshafeinberg.trainvstaxi.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.joshafeinberg.constants.Constants;
import com.joshafeinberg.trainvstaxi.GMHApplication;
import com.joshafeinberg.trainvstaxi.R;

import java.util.List;

public class LoadingActivity extends Activity implements DataApi.DataListener, GoogleApiClient.ConnectionCallbacks{

    private final static String TAG = "WearableGMH";

    private GoogleApiClient mGoogleApiClient;

    private TextView mLoadingView;

    private Boolean hasGotData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_screen);

        mLoadingView = (TextView) findViewById(R.id.text);
        Animation loadAnimation = AnimationUtils.loadAnimation(this, R.anim.loading);
        mLoadingView.setAnimation(loadAnimation);

        mGoogleApiClient = GMHApplication.getApiClient();
        mGoogleApiClient.registerConnectionCallbacks(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (null != mGoogleApiClient && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.unregisterConnectionCallbacks(this);
            Wearable.DataApi.removeListener(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }



    @Override
    public void onConnected(Bundle connectionHint) {
        Wearable.DataApi.addListener(mGoogleApiClient, this);
        fireRequest();
    }
    @Override
    public void onConnectionSuspended(int cause) {
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEvents);
        dataEvents.close();

        // data events were returning multiple times so this blocks the additional data
        if (hasGotData) {
            return;
        }

        for (DataEvent event : events) {
            if (event.getType() == DataEvent.TYPE_DELETED) {
                Log.d(TAG, "DataItem deleted: " + event.getDataItem().getUri());
            } else if (event.getType() == DataEvent.TYPE_CHANGED) {
                String path = event.getDataItem().getUri().getPath();

                if (Constants.containsPath(Constants.SET_LOCATION_ERROR, path)) {
                    mLoadingView.setAnimation(null);
                    mLoadingView.setText(getString(R.string.set_location_error));
                } else if (Constants.containsPath(Constants.GENERIC_ERROR, path)) {
                    mLoadingView.setAnimation(null);
                    mLoadingView.setText(getString(R.string.generic_error));
                } else if (Constants.containsPath(Constants.SEND_DATA, path)) {
                    Log.i(TAG, "Got Data");
                    hasGotData = true;
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
                    loadData(dataMapItem);
                }

                Log.d(TAG, "DataItem changed: " + path);
            }
        }
    }

    public void fireRequest() {
        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(String.format(Constants.REQUEST_DATA, System.currentTimeMillis()));
        PutDataRequest request = putDataMapRequest.asPutDataRequest();

        Wearable.DataApi.putDataItem(mGoogleApiClient, request)
                .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                    @Override
                    public void onResult(DataApi.DataItemResult dataItemResult) {
                        Log.d(TAG, "putDataItem status: "
                                + dataItemResult.getStatus().toString());
                    }
                });
    }

    private void loadData(DataMapItem dataMapItem) {
        mLoadingView.setAnimation(null);

        GridActivity.dataMap = dataMapItem.getDataMap();
        startActivity(new Intent(this, GridActivity.class));

        finish();
    }
}
