package com.joshafeinberg.trainvstaxi;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static Context mContext;

    // app keys
    public static final String UBER_CLIENT_ID = "";
    public static final String UBER_API_KEY = "";
    public static final String GOOGLE_API_KEY = "";

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext(){
        return mContext;
    }
}