package com.joshafeinberg.trainvstaxi.models;


import android.net.Uri;

public class SimpleLocation {

    private String address;
    private Double latitude;
    private Double longitude;

    public SimpleLocation(String address, Double latitude, Double longitude) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getFormattedAddress() {
        return Uri.encode(getAddress());
    }
}
