package com.joshafeinberg.trainvstaxi.models;

public abstract class ParentData {
    public enum DATA_TYPE {UBER, CTA}

    public abstract DATA_TYPE getType();
    public abstract String getDisplayName();
    public abstract Integer getArrivalTime();
    public abstract Integer getTravelTime();

    public boolean isUber() {
        return getType().equals(DATA_TYPE.UBER);
    }

    public boolean isCTA() {
        return getType().equals(DATA_TYPE.CTA);
    }
}
