package com.joshafeinberg.trainvstaxi.models.uber;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UberPrice {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @Expose
    private String estimate;
    @SerializedName("low_estimate")
    @Expose
    private Integer lowEstimate;
    @SerializedName("high_estimate")
    @Expose
    private Integer highEstimate;
    @SerializedName("surge_multiplier")
    @Expose
    private Double surgeMultiplier;
    @Expose
    private Integer duration;

    public String getProductId() {
        return productId;
    }

    public String getEstimate() {
        return estimate;
    }

    public Integer getDuration() {
        return duration;
    }
}
