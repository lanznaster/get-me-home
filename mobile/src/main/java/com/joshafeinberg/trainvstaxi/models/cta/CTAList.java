package com.joshafeinberg.trainvstaxi.models.cta;

import java.util.List;

public class CTAList {

    private List<CTAData> ctaDataList;
    private Boolean isComplete = false;
    private Long lastUpdateTime;

    public List<CTAData> getCtaDataList() {
        return ctaDataList;
    }

    public void setCtaDataList(List<CTAData> ctaDataList) {
        this.ctaDataList = ctaDataList;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete() {
        isComplete = true;
    }

    public Long getLastUpdateTime() {
        if (lastUpdateTime == null) {
            lastUpdateTime = 0L;
        }
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

}
