package com.joshafeinberg.trainvstaxi.models.google;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Step {

    public static final String TRANSIT = "TRANSIT";
    public static final String WALKING = "WALKING";

    @SerializedName("travel_mode")
    @Expose
    private String travelMode;
    @SerializedName("transit_details")
    @Expose
    private TransitDetail transitDetail;

    public Boolean isWalking() {
        return travelMode.equalsIgnoreCase(WALKING);
    }

    public SpannableString getDisplayString() {

        SpannableString error = SpannableString.valueOf("ERROR");

        if (travelMode.equalsIgnoreCase(WALKING)) {
            return SpannableString.valueOf("Walk");
        }

        if (travelMode.equalsIgnoreCase(TRANSIT)) {
            if (transitDetail == null) {
                return error;
            }
            if (transitDetail.getLine() == null) {
                return error;
            }

            Line line = transitDetail.getLine();

            if (line.getColor() != null) {
                // this is a CTA line
                SpannableString lineName = new SpannableString(line.getName());
                lineName.setSpan(new ForegroundColorSpan(Color.parseColor(line.getColor())), 0, lineName.length(), 0);
                return lineName;
            }

            if (line.getShortName() != null) {
                // this is a bus route
                return SpannableString.valueOf(line.getShortName() + " (" + line.getName() + ")");
            }
        }

        return error;
    }

    public Long getArrivalTime() {
        if (transitDetail == null) {
            return 0L;
        }

        Time arrivalTime = transitDetail.getArrivalTime();
        if (arrivalTime == null) {
            return 0L;
        }

        return arrivalTime.getValue();
    }
}
