package com.joshafeinberg.trainvstaxi.models.uber;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UberTime {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @Expose
    private Integer estimate;

    public String getProductId() {
        return productId;
    }

    public Integer getEstimate() {
        return estimate;
    }
}
