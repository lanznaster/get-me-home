package com.joshafeinberg.trainvstaxi.models.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransitDetail {

    @Expose
    private Line line;
    @SerializedName("departure_time")
    @Expose
    private Time departureTime;

    public Line getLine() {
        return line;
    }

    public Time getArrivalTime() {
        return departureTime;
    }
}
