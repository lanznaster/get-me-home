package com.joshafeinberg.trainvstaxi.models.google;

import com.google.gson.annotations.Expose;

public class Prediction {

    @Expose
    private String description;

    public String getDescription() {
        return description;
    }
}
