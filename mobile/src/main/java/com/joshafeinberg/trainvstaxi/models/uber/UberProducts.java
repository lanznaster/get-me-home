package com.joshafeinberg.trainvstaxi.models.uber;


import com.google.gson.annotations.Expose;

import java.util.List;

public class UberProducts {

    @Expose
    List<UberProduct> products;

    public List<UberProduct> getProducts() {
        return products;
    }
}
