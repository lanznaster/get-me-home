package com.joshafeinberg.trainvstaxi.models.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Legs {

    @Expose
    private KeyValue duration;
    @SerializedName("start_address")
    @Expose
    private String startAddress;
    @Expose
    private List<Step> steps;

    public KeyValue getDuration() {
        return duration;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public List<Step> getSteps() {
        return steps;
    }
}
