package com.joshafeinberg.trainvstaxi.models.uber;

import com.google.gson.annotations.Expose;

import java.util.List;


public class UberTimes {
    @Expose
    private List<UberTime> times;

    public List<UberTime> getTimes() {
        return times;
    }
}
