package com.joshafeinberg.trainvstaxi.models.google;

import com.google.gson.annotations.Expose;

import java.util.List;

public class Predictions {

    @Expose
    private List<Prediction> predictions;

    public List<Prediction> getPredictions() {
        return predictions;
    }
}
