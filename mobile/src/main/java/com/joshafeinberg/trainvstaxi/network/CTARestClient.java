package com.joshafeinberg.trainvstaxi.network;

import java.util.Map;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.QueryMap;

public interface CTARestClient {

    @GET("/api/1.0/ttarrivals.aspx")
    Response getCTAArrivalTime(@QueryMap Map<String, String> options);

}
