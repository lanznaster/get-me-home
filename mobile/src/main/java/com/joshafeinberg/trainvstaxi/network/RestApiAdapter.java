package com.joshafeinberg.trainvstaxi.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class RestApiAdapter {

    private static final String GOOGLE_URL = "https://maps.googleapis.com";
    private static final String CTA_URL = "http://lapi.transitchicago.com";
    private static final String UBER_URL = "https://api.uber.com";

    private static RestApiAdapter sInstance;

    protected RestAdapter mGoogleAdapter = null;
    protected GoogleRestClient mGoogleClient = null;

    protected RestAdapter mUberAdapter = null;
    protected UberRestClient mUberClient = null;

    public static RestApiAdapter getInstance() {
        if (sInstance == null) {
            sInstance = new RestApiAdapter();
            sInstance.createAdapter();
        }
        return sInstance;
    }

    protected void createAdapter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = gsonBuilder.create();

        mGoogleAdapter = new RestAdapter.Builder()
                .setEndpoint(GOOGLE_URL)
                .setConverter(new GsonConverter(gson))
                .build();

        mGoogleClient = mGoogleAdapter.create(GoogleRestClient.class);

        mUberAdapter = new RestAdapter.Builder()
                .setEndpoint(UBER_URL)
                .setConverter(new GsonConverter(gson))
                .build();

        mUberClient = mUberAdapter.create(UberRestClient.class);
    }

    public GoogleRestClient getGoogleRestClient() {
        return mGoogleClient;
    }

    public UberRestClient getUberRestClient() {
        return mUberClient;
    }

}
