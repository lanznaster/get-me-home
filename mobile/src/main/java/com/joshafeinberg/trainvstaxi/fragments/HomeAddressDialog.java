package com.joshafeinberg.trainvstaxi.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Address;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;

import com.joshafeinberg.trainvstaxi.R;
import com.joshafeinberg.trainvstaxi.adapters.PlacesAutoCompleteAdapter;
import com.joshafeinberg.trainvstaxi.models.SimpleLocation;
import com.joshafeinberg.trainvstaxi.models.google.Prediction;
import com.joshafeinberg.trainvstaxi.models.google.Predictions;
import com.joshafeinberg.trainvstaxi.tasks.GetAutoSuggestTask;
import com.joshafeinberg.trainvstaxi.utils.LocationUtils;

import java.util.ArrayList;

public class HomeAddressDialog extends DialogFragment implements GetAutoSuggestTask.OnCompleteListener {

    public static String PREFS_HOME_ADDRESS = "pref_address";
    public static String PREFS_LATITUDE = "pref_latitude";
    public static String PREFS_LONGITUDE = "pref_longitude";

    private AutoCompleteTextView mHomeAddressACTV;
    private NewAddressSetListener mListener;

    public interface NewAddressSetListener {
        void newAddressSet();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.drawable.ic_menu_home);
        builder.setTitle(R.string.sethome);

        View rootView = inflater.inflate(R.layout.set_home_dialog, null, false);

        final SimpleLocation simpleLocation = LocationUtils.getDestination(getActivity());
        mHomeAddressACTV = (AutoCompleteTextView) rootView.findViewById(R.id.homeAddress);
        if (simpleLocation != null) {
            mHomeAddressACTV.setText(simpleLocation.getAddress());
        }

        mHomeAddressACTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                GetAutoSuggestTask getAutoSuggestTask = new GetAutoSuggestTask();
                getAutoSuggestTask.setOnCompleteListener(HomeAddressDialog.this);
                getAutoSuggestTask.execute(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        builder.setView(rootView);

        builder.setPositiveButton(R.string.sethome, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SharedPreferences.Editor editor = preferences.edit();
                if (mHomeAddressACTV.getText() != null) {

                    String newHomeAddress = mHomeAddressACTV.getText().toString();

                    Address destinationAddress = LocationUtils.getDestinationAddress(getActivity(), newHomeAddress);

                    editor.putString(PREFS_HOME_ADDRESS, newHomeAddress);
                    if (destinationAddress != null) {
                        editor.putString(PREFS_LATITUDE, Double.toString(destinationAddress.getLatitude()));
                        editor.putString(PREFS_LONGITUDE, Double.toString(destinationAddress.getLongitude()));
                    }
                    editor.apply();

                    String oldAddress = "";
                    if (simpleLocation != null) {
                        oldAddress = simpleLocation.getAddress();
                    }
                    if (!newHomeAddress.equalsIgnoreCase(oldAddress)) {
                        if (mListener != null) {
                            mListener.newAddressSet();
                        }
                    }
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, null);

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (getParentFragment() instanceof NewAddressSetListener) {
            mListener = (NewAddressSetListener) getParentFragment();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSuggestionsReturned(Predictions predictions) {
        ArrayList<String> strings = new ArrayList<String>();
        for (Prediction prediction : predictions.getPredictions()) {
            strings.add(prediction.getDescription());
        }

        PlacesAutoCompleteAdapter adapter = new PlacesAutoCompleteAdapter(getActivity(), android.R.layout.simple_dropdown_item_1line);
        adapter.setResultList(strings);

        mHomeAddressACTV.setAdapter(adapter);
    }
}
