package com.joshafeinberg.trainvstaxi.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.R;
import com.joshafeinberg.trainvstaxi.models.SimpleLocation;
import com.joshafeinberg.trainvstaxi.models.cta.CTAData;
import com.joshafeinberg.trainvstaxi.models.cta.CTAList;
import com.joshafeinberg.trainvstaxi.models.google.Routes;
import com.joshafeinberg.trainvstaxi.models.google.TravelTime;
import com.joshafeinberg.trainvstaxi.models.uber.UberData;
import com.joshafeinberg.trainvstaxi.models.uber.UberPrice;
import com.joshafeinberg.trainvstaxi.models.uber.UberPrices;
import com.joshafeinberg.trainvstaxi.models.uber.UberProduct;
import com.joshafeinberg.trainvstaxi.models.uber.UberProducts;
import com.joshafeinberg.trainvstaxi.models.uber.UberTime;
import com.joshafeinberg.trainvstaxi.models.uber.UberTimes;
import com.joshafeinberg.trainvstaxi.tasks.GetTravelTimeTask;
import com.joshafeinberg.trainvstaxi.tasks.GetUberPricesTask;
import com.joshafeinberg.trainvstaxi.tasks.GetUberProductsTask;
import com.joshafeinberg.trainvstaxi.tasks.GetUberTimesTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DataLookup implements GetUberProductsTask.OnCompleteListener,
                                   GetUberPricesTask.OnCompleteListener,
                                   GetUberTimesTask.OnCompleteListener,
                                   GetTravelTimeTask.OnCompleteListener {

    public interface DataLookupInterface {
        public void complete();
        public void showErrorMessage(int message);
        public void locationServicesOff();
        public void handleBadLocation();
        public void handleNoDestination();
    }

    private static final int REFRESH_TIME = 60; // 1 minute
    private static final int VALID_LOCATION_UPDATE_TIME = 300; // 5 minutes

    private Context mContext = App.getContext();

    private DataLookupInterface mInterface;

    private Location mCurrentLocation;
    private SimpleLocation mDestination;

    private HashMap<String, UberData> mDataHashMap;
    private CTAList mCTAData;

    public DataLookup(DataLookupInterface lookupInterface) {
        mDestination = LocationUtils.getDestination(mContext);
        mInterface = lookupInterface;

        mCTAData = new CTAList();
    }

    public void getLocationData() {
        final LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                lm.removeUpdates(this);
                onLocationFound(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.e("trainvtaxi", "provider status changed");
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.e("trainvtaxi", "provider enabled");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.e("trainvtaxi", "provider disabled");
            }
        };
        Location lastGPSLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location lastNetworkLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (isValidLastLocation(lastGPSLocation)) {
            onLocationFound(lastGPSLocation);
        } else if (isValidLastLocation(lastNetworkLocation)) {
            onLocationFound(lastNetworkLocation);
        } else if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(Criteria.POWER_LOW);
            String locationProvider = lm.getBestProvider(criteria, true);
            lm.requestSingleUpdate(locationProvider, locationListener, null);
        } else {
            mInterface.locationServicesOff();
        }
    }

    private boolean isValidLastLocation(Location location) {
        if (location == null) {
            return false;
        }

        long validTime = System.currentTimeMillis() - VALID_LOCATION_UPDATE_TIME;

        return location.getTime() < validTime;
    }


    public void onLocationFound(Location location) {
        mCurrentLocation = location;
        Double longitude = location.getLongitude();
        Double latitude = location.getLatitude();

        GetUberProductsTask getUberProductsTask = new GetUberProductsTask();
        getUberProductsTask.setOnCompleteListener(this);
        getUberProductsTask.execute(longitude, latitude);

        // limits the updates to not use up api call limit
        long currentTime = System.currentTimeMillis() / 1000;
        if (currentTime > mCTAData.getLastUpdateTime() + REFRESH_TIME) {
            getTravelTime();
        }
    }

    /**
     * uses google api to calculate the time in seconds to travel
     */
    private void getTravelTime() {
        if (mDestination == null) {
            mInterface.handleNoDestination();
            return;
        }

        GetTravelTimeTask getTravelTimeTask = new GetTravelTimeTask(mCurrentLocation);
        getTravelTimeTask.setOnCompleteListener(this);
        getTravelTimeTask.execute(mDestination.getAddress());
    }

    @Override
    public void onTravelTimeFetched(TravelTime travelTime) {

        if (travelTime == null) {
            Log.e("trainvtaxi", "travelTime => null");
            mInterface.showErrorMessage(R.string.error_public_travel_time);
            return;
        }

        if (!travelTime.isStatusOkay()) {
            Log.e("trainvtaxi", "status => !okay");
            mInterface.showErrorMessage(R.string.error_public_travel_time);
            return;
        }

        List<CTAData> ctaDataList = new ArrayList<CTAData>();

        for (Routes route : travelTime.getRoutes()) {
            CTAData ctaData = new CTAData();
            ctaData.setTravelTime(route.getRouteTime());
            ctaData.setRoute(route.getDisplayString());
            Long arrivalTime = route.getArrivalTime() - (System.currentTimeMillis() / 1000);
            ctaData.setArrivalTime(arrivalTime.intValue());
            ctaData.setStartAddress(route.getStartAddress());
            ctaDataList.add(ctaData);
        }

        mCTAData.setLastUpdateTime(System.currentTimeMillis() / 1000);
        mCTAData.setCtaDataList(ctaDataList);
        mCTAData.setComplete();

        mInterface.complete();
    }

    @Override
    public void onProductsFetched(UberProducts response) {
        mDataHashMap = new HashMap<String, UberData>();

        if (response == null) {
            Log.e("trainvtaxi", "products => null");
            mInterface.showErrorMessage(R.string.error_uber);
            return;
        }

        for (UberProduct product : response.getProducts()) {
            UberData uberData = new UberData();
            uberData.setProductId(product.getProductId());
            uberData.setDisplayName(product.getDisplayName());
            uberData.setCapacity(product.getCapacity());
            uberData.setImage(product.getImage());
            mDataHashMap.put(product.getProductId(), uberData);
        }

        Double longitude = mCurrentLocation.getLongitude();
        Double latitude = mCurrentLocation.getLatitude();

        Double destinationLongitude;
        Double destinationLatitude;
        if (mDestination != null) {
            destinationLongitude = Double.parseDouble(mDestination.getLongitude().toString());
            destinationLatitude = Double.parseDouble(mDestination.getLatitude().toString());
        } else {
            Log.e("trainvtaxi", "bad location data");
            handleBadLocation();
            return;
        }

        GetUberPricesTask getUberPricesTask = new GetUberPricesTask();
        getUberPricesTask.setOnCompleteListener(this);
        getUberPricesTask.execute(longitude, latitude, destinationLongitude, destinationLatitude);
    }

    private void handleBadLocation() {
        mInterface.handleBadLocation();
    }

    @Override
    public void onPricesFetched(UberPrices response) {
        if (response == null) {
            Log.e("trainvtaxi", "prices => null");
            mInterface.showErrorMessage(R.string.error_uber_price);
            for (UberData uberData : mDataHashMap.values()) {
                uberData.setPriceEstimate(mContext.getString(R.string.price_unavailable));
                uberData.setDuration(-1);
            }
        } else {
            for (UberPrice price : response.getPrices()) {
                UberData uberData = mDataHashMap.get(price.getProductId());
                if (uberData != null) {
                    uberData.setPriceEstimate(price.getEstimate());
                    uberData.setDuration(price.getDuration());
                }
            }
        }

        Double longitude = mCurrentLocation.getLongitude();
        Double latitude = mCurrentLocation.getLatitude();
        GetUberTimesTask getUberTimesTask = new GetUberTimesTask();
        getUberTimesTask.setOnCompleteListener(this);
        getUberTimesTask.execute(longitude, latitude);
    }

    @Override
    public void onTimesFetched(UberTimes response) {
        if (response == null) {
            Log.e("trainvtaxi", "times => null");
            mInterface.showErrorMessage(R.string.error_uber);
            mDataHashMap.clear(); // data is basically useless without time
            return;
        }

        for (UberTime time : response.getTimes()) {
            UberData uberData = mDataHashMap.get(time.getProductId());
            if (uberData != null) {
                uberData.setTimeEstimate(time.getEstimate());
                uberData.setComplete();
            }
        }

        mInterface.complete();
    }

    public HashMap<String, UberData> getDataHashMap() {
        return mDataHashMap;
    }

    public void setDataHashMap(HashMap<String, UberData> mDataHashMap) {
        this.mDataHashMap = mDataHashMap;
    }

    public CTAList getCTAData() {
        return mCTAData;
    }

    public void setCTAData(CTAList mCTAData) {
        this.mCTAData = mCTAData;
    }

    public void updateDestination() {
        mDestination = LocationUtils.getDestination(mContext);
    }
}
