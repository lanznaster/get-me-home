package com.joshafeinberg.trainvstaxi.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.joshafeinberg.trainvstaxi.fragments.HomeAddressDialog;
import com.joshafeinberg.trainvstaxi.models.SimpleLocation;

import java.util.List;


public class LocationUtils {

    public static SimpleLocation getDestination(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String destination = preferences.getString(HomeAddressDialog.PREFS_HOME_ADDRESS, "");
        Double latitude = Double.valueOf(preferences.getString(HomeAddressDialog.PREFS_LATITUDE, "0"));
        Double longitude = Double.valueOf(preferences.getString(HomeAddressDialog.PREFS_LONGITUDE, "0"));

        if (TextUtils.isEmpty(destination) || latitude == 0F || longitude == 0F) {
            return null;
        } else {
            return new SimpleLocation(destination, latitude, longitude);
        }
    }

    public static Address getDestinationAddress(Context context, String addressString) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        Address destination = null;

        try {
            address = coder.getFromLocationName(addressString,5);
            if (address != null && address.size() > 0) {
                destination = address.get(0);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return destination;
    }

}
