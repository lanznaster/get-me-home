package com.joshafeinberg.trainvstaxi.tasks;

import android.os.AsyncTask;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.models.uber.UberPrices;
import com.joshafeinberg.trainvstaxi.network.RestApiAdapter;
import com.joshafeinberg.trainvstaxi.network.UberRestClient;

import java.util.LinkedHashMap;

import retrofit.RetrofitError;

public class GetUberPricesTask extends AsyncTask<Double, Void, UberPrices> {

    public static interface OnCompleteListener {
        public void onPricesFetched(UberPrices response);
    }

    private OnCompleteListener mOnCompleteListener;

    public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
        mOnCompleteListener = onCompleteListener;
    }

    @Override
    protected UberPrices doInBackground(Double... params) {
        Double longitude = params[0];
        Double latitude = params[1];
        Double currentLongitude = params[2];
        Double currentLatitude = params[3];

        UberRestClient uberRestClient = RestApiAdapter.getInstance().getUberRestClient();

        LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
        options.put("server_token", App.UBER_API_KEY);
        options.put("start_longitude", Double.toString(longitude));
        options.put("start_latitude", Double.toString(latitude));
        options.put("end_longitude", Double.toString(currentLongitude));
        options.put("end_latitude", Double.toString(currentLatitude));

        UberPrices uberPrices;
        try {
            uberPrices = uberRestClient.getUberPrices(options);
        } catch (RetrofitError e) {
            // 422 is returned if distance is too far for Uber
            e.printStackTrace();
            uberPrices = null;
        }
        return uberPrices;
    }

    @Override
    protected void onPostExecute(UberPrices response) {
        mOnCompleteListener.onPricesFetched(response);
    }
}
