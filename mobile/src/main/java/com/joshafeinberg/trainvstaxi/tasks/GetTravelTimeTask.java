package com.joshafeinberg.trainvstaxi.tasks;

import android.location.Location;
import android.os.AsyncTask;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.models.google.TravelTime;
import com.joshafeinberg.trainvstaxi.network.GoogleRestClient;
import com.joshafeinberg.trainvstaxi.network.RestApiAdapter;

import java.util.LinkedHashMap;

public class GetTravelTimeTask extends AsyncTask<String, Void, TravelTime> {

    public static interface OnCompleteListener {
        public void onTravelTimeFetched(TravelTime travelTime);
    }

    private static final String TRANSIT = "transit";

    private OnCompleteListener mOnCompleteListener;
    private Location mLocation;

    public GetTravelTimeTask(Location location) {
        mLocation = location;
    }

    public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
        mOnCompleteListener = onCompleteListener;
    }

    @Override
    protected TravelTime doInBackground(String... params) {
        String destination = params[0];

        GoogleRestClient googleRestClient = RestApiAdapter.getInstance().getGoogleRestClient();

        LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
        options.put("origin", mLocation.getLatitude() + "," + mLocation.getLongitude());
        options.put("destination", destination);
        options.put("sensor", "true");
        options.put("mode", TRANSIT);
        options.put("alternatives", "true");
        options.put("departure_time", Long.toString(System.currentTimeMillis() / 1000)); // convert the milliseconds
        options.put("key", App.GOOGLE_API_KEY);


        return googleRestClient.getGoogleMapDirections(options);
    }

    @Override
    protected void onPostExecute(TravelTime travelTime) {
        mOnCompleteListener.onTravelTimeFetched(travelTime);
    }
}
